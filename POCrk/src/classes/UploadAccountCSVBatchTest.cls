@isTest
public class UploadAccountCSVBatchTest {
    static testMethod void uploadAccount(){
        String fileString = 'Name,Account Number,Street,City,State,Zip Code,Country,Phone \n'
            + 'Account Upload,A234519,TestStreet,TestCity,TestState,7896,TestCountry,9999999 \n';
        
        Blob bodyBlob = Blob.valueOf(fileString);
        
        ContentVersion testContentInsert = new ContentVersion(); 
 		testContentInsert.Title = 'AccountLoad';
        testContentInsert.VersionData = bodyBlob;
        testContentInsert.PathOnClient = 'Penguins.jpg';
 		insert testContentInsert; 
        
        Test.startTest();
        system.schedule('Account File Upload', '0 24 12 * * ?', new UploadAccountCSVBatch());
        Test.stopTest();
    }
}