/* 
 *	Class : UploadAccountCSVBatch
 *	Author : Krishna C J
 *	Date : 05-04-2017
 *	Description : This class will parse the .csv file from salesforce file sync and creates/updates Accounts  

 *	To schedule:  
 		system.schedule('Account File Upload', '0 24 12 * * ?', new UploadAccountCSVBatch());
*/
global class UploadAccountCSVBatch implements Database.Batchable<String>, Schedulable, Database.Stateful {
    private String csvBatch;
    private Id recId;
    
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(this, 200);
	}
	
	global Iterable<String> start(Database.batchableContext ctx){ 
		recId = [SELECT Id FROM ContentDocument WHERE Title = 'AccountLoad' ORDER BY LastModifiedDate DESC LIMIT 1].Id;    
        ContentVersion cntVer = [SELECT Id, VersionData, Title FROM ContentVersion where ContentDocumentId = :recId ORDER BY LastModifiedDate DESC LIMIT 1];
        return new CSVIteratorNew((cntVer.VersionData).toString(), '\n');
    }
    
	global void execute(Database.BatchableContext ctx, list<String> scope){      
        csvBatch = '';
        for(String row : scope){
            csvBatch += row + '\n';
        }
        importAccountCSVFile();
    }
    
    global void finish(Database.BatchableContext ctx) {                 
        ContentDocument cntDoc = [SELECT Id, Title FROM ContentDocument WHERE Id = :recId];
        //delete cntDoc;
    }
    
    private void importAccountCSVFile(){            
		list<String> accNameList = new list<String>();	
		list<Account> tempAccountList = new list<Account>();		
		list<Account> insertAccountList = new list<Account>();
		list<Account> updateAccountList = new list<Account>(); 
		map<String, Id> accNameIdMap = new map<String, Id>();
		
		list<String> csvFileLines = csvBatch.split('\n');
		
        csvFileLines.remove(0);
        for(Integer i=0; i<csvFileLines.size(); i++){
			Account accObj = new Account();             
			list<String> csvRecordData = csvFileLines[i].split(',');
			
			if(csvRecordData[0].trim() != null && csvRecordData[0].trim() != ''){
				accObj.Name = csvRecordData[0];
				accNameList.add(csvRecordData[0]);
				
				if(csvRecordData[1] != null){
					accObj.AccountNumber = csvRecordData[1];
				}			
				if(csvRecordData[2].trim() != null && csvRecordData[2].trim() != ''){
					accObj.BillingStreet = csvRecordData[2];
				}							
				if(csvRecordData[3].trim() != null && csvRecordData[3].trim() != ''){
					accObj.BillingCity = csvRecordData[3];
				}
                if(csvRecordData[4].trim() != null && csvRecordData[4].trim() != ''){
					accObj.BillingState = csvRecordData[4]; 
				}
						   
				if(csvRecordData[5].trim() != null && csvRecordData[5].trim() != ''){
					if(csvRecordData[5].length() == 5){
						accObj.BillingPostalCode = csvRecordData[5];
					}
					else {
						if(csvRecordData[5].length() == 4){
						   accObj.BillingPostalCode = '0' + csvRecordData[5]; 
						}
						if(csvRecordData[5].length() == 3){
						   accObj.BillingPostalCode = '00' + csvRecordData[5]; 
						}					
						if(csvRecordData[5].length() == 2){
						   accObj.BillingPostalCode = '000' + csvRecordData[5]; 
						}
						if(csvRecordData[5].length() == 1){
						   accObj.BillingPostalCode = '0000' + csvRecordData[5]; 
						}
					}
				}
					
				if(csvRecordData[6].trim() != null && csvRecordData[6].trim() != ''){
					accObj.BillingCountry = csvRecordData[6];
				}
				if(csvRecordData[7].trim() != null && csvRecordData[7].trim() != ''){
					accObj.Phone = csvRecordData[7];
				}    
				tempAccountList.add(accObj); 
			}
        }   
        
		if(!tempAccountList.isEmpty()){
            insert tempAccountList;
			/*for(Account accTemp : [SELECT Id, Name FROM Account WHERE Name IN :accNameList]){
				if(accTemp != NULL){
					accNameIdMap.put(accTemp.Name, accTemp.Id);
				}			
			}
			
			for(Account acc : tempAccountList){
				if(accNameIdMap != NULL && accNameIdMap.containsKey(acc.Name)){ 
					acc.Id = accNameIdMap.get(acc.Name);
					updateAccountList.add(acc);
				}
				else {
					insertAccountList.add(acc);
				}
			} 
				
			if(!insertAccountList.isEmpty()){
				insert insertAccountList;
			}
				
			if(!updateAccountList.isEmpty()){
				update updateAccountList;
			}*/
		}	                     
	}    
}