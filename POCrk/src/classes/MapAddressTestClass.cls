@isTest
private class MapAddressTestClass {
  
    public static testMethod void testMyController() {
    
     Integer pagecnt,recordcnt;
         boolean shownext,showprev;
        MapAddress controller = new MapAddress();
      
        ApexPages.currentPage().getParameters().put('qp', 'yyyy');
         
        List<Account> accts = new List<Account>{};
                For(Integer x=0;x<200;x++){
                   Account a = new Account(name='test Account ' + x);
                   accts.add(a);
                }
         insert accts; 
      
       List<Contact> conList = new List<Contact>{};
    
                For(Integer x=0;x<200;x++){
                   Contact a = new Contact(firstname='first name ' + x,lastname='last name' + x);
                   conList.add(a);
                }
         insert conList; 
        
      List<SelectOption> selectopt=controller.getAccountList();
      //Invoke service
        if(controller.invokeService()==null)
        {
             
        }
      
      controller.accCount=0;
      List<SelectOption> selectopt1=controller.getAccountList();
    
         
      controller.setAccountInfo('sForce');
      String accInfo=controller.getAccountInfo();
      System.assertEquals('sForce', accInfo);
        if(controller.invokeService()==null)
        {
             
        }
        //if all condition portion of invoke Service 
        controller.accountInfo='0';
         System.assertEquals('0', controller.accountInfo);
        if(controller.invokeService()==null)
        {
        }
        
        controller.index=100;
         System.assertEquals(100,  controller.index);
      /*  if(controller.invokeService()==null)
        {
             
        }*/

        List<MapAddress.contactwrapper> totalcontacts=controller.getContacts();
        
         for(MapAddress.contactwrapper contwrapper : totalcontacts)
         {
              contwrapper.selected=true;
              controller.contactList.add(contwrapper);
              System.assertEquals(true, controller.contactList.get(0).selected);
         }
       
       if(controller.contactList!=null)
       {
        if(controller.getSelected()==null)
        {
        }
       }
        //if not null getSelectedcontacts
       controller.index=0;
        String[] invokestring=controller.GetSelectedContacts();
        
        controller.contactList_Next.clear();
        List<MapAddress.contactwrapper> totalcontacts1=controller.getContacts();
        
        if(controller.getSelected()==null)
        {
            System.assertEquals(null, controller.getSelected());
            controller.selectedContacts.clear();
        }
        //if not null getSelectedcontacts
        String[] invokestring1=controller.GetSelectedContacts();
        
        //paging
        Integer pageno=controller.getPageNo();
        controller.Next();
          controller.Prev();
          shownext=controller.getshownext();
          showprev=controller.getshowprev();
          pagecnt=controller.getPageCount();
          recordcnt=controller.getRecordCount();
          
          
        controller.next=1;
        System.assertEquals(1, controller.next);
          controller.count=1;
         System.assertEquals(1, controller.count);
          pagecnt=controller.getPageCount();
          recordcnt=controller.getRecordCount();
         
          controller.next=0;
           System.assertEquals(0, controller.next);
          controller.count=0;
          System.assertEquals(0, controller.count);
          pagecnt=controller.getPageCount();
          recordcnt=controller.getRecordCount();
          
          
          controller.next=3;
          controller.count=3;
          controller.pageCnt=10;
          controller.pageLimit=10;
          pagecnt=controller.getPageCount();
          recordcnt=controller.getRecordCount();
          
           controller.next=3;
          controller.count=5;
          
          controller.pageCnt=2;
          controller.pageLimit=2;
          pagecnt=controller.getPageCount();
          recordcnt=controller.getRecordCount();
          
           controller.pageCnt=0;
           controller.pageLimit=0;
          pagecnt=controller.getPageCount();
          recordcnt=controller.getRecordCount();
          
                   
           controller.pageCnt=1;
           controller.pageLimit=1;
          pagecnt=controller.getPageCount();
          recordcnt=controller.getRecordCount();
          
          controller.next=5;
          controller.count=5;
          controller.Next();
          shownext=controller.getshownext();
          controller.Prev();
          showprev=controller.getshowprev();
          
          controller.contactList.clear();
          controller.Next();
          shownext=controller.getshownext();
           controller.Prev();
          showprev=controller.getshowprev();
          
          controller.next=10;
          controller.count=2;
           controller.Prev();
           controller.pageLimit=-1;
           recordcnt=controller.getRecordCount();
           
           
           controller.pageLimit=-10;
           Integer pageno1=controller.getPageNo();
          String chartString=controller.getChartData();
          
      
          
    

      }
      }