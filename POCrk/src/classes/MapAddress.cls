public with sharing class MapAddress
{
   private String qp;
   public MapAddress()
    {
        this.qp = ApexPages.currentPage().getParameters().get('qp');
    }
        public String accountInfo;
      
        Contact[] contactArray=new Contact[100];
        
        public Integer next = 10, count = 10,pageCnt=0,pageLimit=0,index;
        public boolean shownext, showprev;

        public String getAccountInfo() { return this.accountInfo; }
        public void setAccountInfo(String s) { this.accountInfo = s; }

        public List<contactwrapper> contactList = new List<contactwrapper>();
       public List<contactwrapper> contactList_Next = new List<contactwrapper>();
       public List<Contact> selectedContacts = new List<Contact>();
       
 public Integer accCount=[SELECT count() from Account];

        //To Create Wrapper class for Contacts
        public class contactwrapper
         {
             public Contact cont { get; set; }
             public Boolean selected {get; set;}
             public contactwrapper(Contact c)
             {
                 cont=c;
                 selected=false;
             }
        }
          
        //To Select the Account detail from Account Table
          public List<SelectOption> getAccountList() {
              List<SelectOption> optionList = new List<SelectOption>();
              optionList.add(new SelectOption('-1','- None -'));
              optionList.add(new SelectOption('0','- All -'));
             
              if(accCount>0)
              {              
                  for (Account acnt : [select name from Account order by Name ]){
                    optionList.add(new SelectOption(acnt.id,acnt.name));
                  }
              }
              else
              {
                  optionList.clear();
              }
               return optionList;  
            }
        
        //To fetch the Contact List.
        public List<contactwrapper> getContacts()
        {
            if(contactList_Next.size()>0)
            {
                 return contactList_Next;
             }
             else
             {
                    return null;
              }
        }
        
        //To Fetch the Page Number
        public Integer getPageCount()
        {
            if(count>0)
            {
                if(pageCnt>=count)
                {
                    Double remainder=math.mod(pageCnt,count);
                    if(remainder>0)
                        return (pageCnt/count)+1;
                    else
                        return pageCnt/count;
               }
               else if(pageCnt>0)                    
                return 1;
               else
                return pageCnt;
            }
            else
                return 0;
        }
        //To Fetch the Total Records
        public Integer getRecordCount()
        {
            return pageCnt;
        }
        
        //To get the Total number of Pages
        public Integer getPageNo()
        {
        if(pageLimit>0)
            return pageLimit;
         else
             return 0;
        }
        
        //To Select the Contact Detail in Checkbox.
        public PageReference getSelected()
        {
            selectedContacts.clear();
            for(contactwrapper contwrapper : contactList)
            {
                if(contwrapper.selected==true)
                {
                  selectedContacts.add(contwrapper.cont);
                 }
             }
            return null;
        }
        
        //To Fetch the Selected Contact Details
         public String[] GetSelectedContacts()
        {
             index=0;
             String fulladdr='';
           
            String[] addr=new String[500];
            
            if(selectedContacts.size()>0)
            {
                
                while(index<selectedContacts.size())
                {
                    fulladdr='';
                      contactwrapper cntwrapper=new contactwrapper(selectedContacts.get(index));
                      if(cntwrapper.cont.MailingStreet!=null)
                      {
                          fulladdr=fulladdr+cntwrapper.cont.MailingStreet+'$';
                      }
                      if(cntwrapper.cont.MailingCity!=null)
                      {
                          fulladdr=fulladdr+cntwrapper.cont.MailingCity+'$';
                      }
                      if(cntwrapper.cont.MailingState!=null)
                      {
                            fulladdr=fulladdr+cntwrapper.cont.MailingState+'$';                          
                      }
                      if(cntwrapper.cont.MailingCountry!=null)
                      {
                          fulladdr=fulladdr+cntwrapper.cont.MailingCountry; 
                      }
                      if(cntwrapper.cont.name!=null)
                      {
                         
                         fulladdr = fulladdr+'|'+cntwrapper.cont.name;
                      }
                      
                      //String splittedaddr=fulladdr.replace('"','');
                      String splittedaddr=fulladdr;
                      splittedaddr = splittedaddr.replace('\'', '');
                      addr[index]=splittedaddr;
                      index++;
                 }
                 //addr=addr.replace(''','');acntSaleDet = acntSaleDet.replace('Amount $', '<br>Amount $');
                 
                return addr;
             }
             else
             {
                    return null;
             }
          }
   public String newId;
    //On selection of Account Name Fetch the Contact Details    
    public PageReference invokeService() {
    selectedContacts= new List<Contact>();
    contactList= new List<contactwrapper>();
    contactList_Next= new List<contactwrapper>();
    List<Contact> tempList=new List<Contact>();
    
   //Paging Size
    count = 10;
    pageCnt=0;
    pageLimit=0;
    
   newId= accountInfo;
   if(newId!=null)
   {
   if(newId=='-1')
   {
        //-- none --
        return null;
   }
   else if(newId=='0')//-- All ---
   {
        tempList=[SELECT Id,name,MailingStreet,MailingCity,MailingState,MailingCountry,Email FROM Contact];
         index=0;
         while(index<tempList.size())
         {
         contactList.add(new contactwrapper(tempList.get(index)));
         index++;
         }
                pageCnt=contactList.size();
                if(contactList.size()>count)
                {
                    for(Integer j=0; j<count; j++)
                        contactList_Next.add(contactList[j]);
                    shownext = true;
                }
                else
                {
                    contactList_Next = contactList;
                    shownext = false;
                    showprev=false;
                 }
           if(contactList_Next.size()>0)
                  pageLimit=1;
           
   return null;
   }
   else
   {
       tempList.clear();
       tempList=[SELECT Id,name,MailingStreet,MailingCity,MailingState,MailingCountry,Email FROM Contact WHERE Account.Id=:accountInfo];
       //    Integer i = [select count() from contact where Account.Id=:accountInfo];
        
         index=0;
         while(index<tempList.size())
         {
         contactList.add(new contactwrapper(tempList.get(index)));
         index++;
         }
                pageCnt=contactList.size();
                if(contactList.size()>count)
                {
                    for(Integer j=0; j<count; j++)
                        contactList_Next.add(contactList[j]);
                    shownext = true;
                }
                else
                {
                    contactList_Next = contactList;
                     shownext = false;
                     showprev=false;
                 }
           if(contactList_Next.size()>0)
                  pageLimit=1;

           // }
            
            return null;
           }
           //returning null indicates the same page should be returned - in place change
           }
           else
           {
           return null;
           }
         }

/************************Paging Start***********************/

    Public void Next()
    {
        try
        {
           
            if(shownext==true)
             {
             
              showprev = true;
            contactList_Next.clear();
            Integer limit1 = 0;
            pageLimit=0;
    
            if(next+count < contactList.size())
                limit1 = next+count;
            else
            {
                limit1 = contactList.size();
                shownext = false;
            }
            
            for(Integer i=next; i<limit1; i++)
            contactList_Next.add(contactList[i]);
     
            Next+=count;
            if(contactList.size()>0)
            {
                       pageLimit=Next/count;
             }
             }
        }catch(Exception e){system.debug('Exception :'+e);}
    }
    
    Public void Prev()
    {
        try
        {
           
             if(showprev==true)
             {
              shownext = true;
            contactList_Next.clear();
            Integer limit1 = 0;        
            pageLimit=0;
            if(next-(count+count) > 0)
                limit1 = next-count;
            else
            {
                limit1 = next-count; 
                showprev = false;
            }
           
            for(Integer i=next-(count+count); i<limit1; i++)    
            contactList_Next.add(contactList[i]);
            
            Next-=count; 
             if(contactList.size()>0)
            {
                    pageLimit=Next/count;
             }
            }
        }catch(Exception e){system.debug('Exception :'+e);}               
    }
    
    Public boolean getshownext(){return shownext;}
    
    Public boolean getshowprev(){return showprev;}    

/****************Chart Start**************************/

 public class chartDetails
       {
           public Integer countryCount{get; set; }
           public String countryAddr {get; set;}
           public chartDetails(Integer c,String a)
           {
               countryCount=c;
               countryAddr=a;
               
           }
       }

public String getChartData()
{
 List<chartDetails> countryList = new List<chartDetails>();
        String chartData='';
        String chartPath = 'http://chart.apis.google.com/chart?chtt=Pie+Chart&chts=000000,12&chs=600x200&chf=bg,s,ffffff&cht=p3';
     Integer countryCount=0,m_cnt=0,total_cnt;
     String chd = 't:';//number of country array //23,34,56
     String chl = ''; //array of country name separated by | //Hello|World
     
     if(selectedContacts.size()>0)
     {
         total_cnt=0;
       while(m_cnt<selectedContacts.size())
       {
           countryCount=1;
           contactwrapper cntwrapper=new contactwrapper(selectedContacts.get(m_cnt));
           if(cntwrapper.cont.MailingCountry!=null)
           {
              
                   for(chartDetails ichart : countryList)
                   {    
                       if(ichart.countryAddr==cntwrapper.cont.MailingCountry)
                       {
                           ichart.countryCount++;
                           countryCount++;
                           total_cnt++;
                        }

                   }
              
               if(countryCount==1)
               {
                   countryList.add(new chartDetails(countryCount,cntwrapper.cont.MailingCountry));
                   total_cnt++;
               }
               
           }
           m_cnt++;
        }
        for(chartDetails ichart : countryList)
        {
            Double per=(ichart.countryCount*100)/total_cnt;
            chl=chl+ichart.countryAddr+'('+per+'%)'+'|';
            chd=chd+ichart.countryCount+',';
                 
         }
        
        //remove the last comma or pipe
        if(total_cnt>0)
        {
         chd = chd.substring(0, chd.length() -1);
         chl = chl.substring(0, chl.length() -1);
         String result = '&chd=' + chd + '&chl=' + chl;
         chartData = chartPath +  result;
        }
        else
        {
        chartData = chartPath;
        }
        return chartData;
     }
     else
     {
      return chartPath;
      }
 
}
 
}