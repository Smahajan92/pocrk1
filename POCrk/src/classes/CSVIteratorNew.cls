/* 
 *	Class : CSVIteratorNew
 *	Author : Krishna C J
 *	Date : 04-04-2017
 *	Description : This class will parse the .csv file and iterable  
*/
global class CSVIteratorNew implements Iterator<String>, Iterable<String> {
	private String m_CSVData;
	private String m_introValue;
	
	public CSVIteratorNew(String fileData, String introValue) {
		m_CSVData = fileData;
		m_introValue = '\n'; 
	}
	
	global Boolean hasNext() {
		return m_CSVData.length() > 1 ? true : false;
	}
	
	global String next() {
		String row = m_CSVData.substringBefore('\n');
		m_CSVData = m_CSVData.substringAfter('\n');
		return row;
	}
	
	global Iterator<String> Iterator() {
		return this;   
	}
}