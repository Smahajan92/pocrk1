/*
* Controller to implement the auto-complete feature on the Visualforce page
*/
public with sharing class CustomSchedulerController{
    
    // Instance fields
    public String searchTerm {get; set;}
    public String cron{get; set;}
     public Integer size{get; set;}
    public String selectedMovie {get; set;}
    
    // Constructor
    public CustomSchedulerController() {
        
    }
    
    public void Schedule(){
    
        String sch = '0 0 13 * * ?';
        String ScheduledJobID = NULL;
        Try{
        
        // Instantiate the batch class
        
 
// Instantiate the scheduler
BatchScheduler scheduler = new BatchScheduler();
 
// Assign the batch class to the variable within the scheduler
Type myType=Type.forName(selectedMovie);
scheduler.batchClass = (Database.Batchable<SObject>)myType.newInstance();
scheduler.batchSize = size;
            
            ScheduledJobID = System.schedule('MyBatch - Everyday at 1pm', cron, scheduler); 
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'There might be another schedule at same time. Please cross check.'));
        }
        if(ScheduledJobID != null){
            size = null;
            cron = null;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Batch ' + selectedMovie  + ' scheduled as requested. Schedule id: ' + ScheduledJobID));
         }
    }
    
    // JS Remoting action called when searching for a movie name
    @RemoteAction
    public static List<ApexClass> searchMovie(String searchTerm) {

        List<ApexClass> movies = Database.query('Select Id, Name from ApexClass where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return movies;
    }
    
    public list<CronTrigger> ls {get; set;}
    public boolean showblock {get; set;}
    public void ViewSchedule(){
    list<id> schedulelist = new list<id>();
    ls = [SELECT Id, CronJobDetailId, CronJobDetail.Name, CronJobDetail.JobType,CronExpression,state FROM CronTrigger];
    if(ls!=null) Showblock = TRUE;
    }
    
}