global class batchAccountUpdate implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
    	// 60732   
    	//200-2000
    	// Relationship queries 
        String query = 'SELECT Id,Name,Age__c FROM Account';
        
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account> scope) {
    	List<account> acc = new list<account>();
    	
         for(Account a : scope)
         {	
             a.Name = a.Name;
             try{
             a.Age__c = 30/a.Age__c;
             
             acc.add(a);
             }
             catch(Exception e){
             	
             } 
             
             
                        
         }
         update acc;         
         
    }   
    
    global void finish(Database.BatchableContext BC) {
    	
    	system.debug('All finished');
    	
    	// Emailing the related receipient
    	// Execute another batch from here -  Batch chaining.
    }
}